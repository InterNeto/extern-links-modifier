<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit62746c3ff80ea64a239317e5911f55bd
{
    public static $prefixLengthsPsr4 = array (
        'I' => 
        array (
            'InterNeto\\ExternLinksModifier\\' => 30,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'InterNeto\\ExternLinksModifier\\' => 
        array (
            0 => __DIR__ . '/../..' . '/includes',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit62746c3ff80ea64a239317e5911f55bd::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit62746c3ff80ea64a239317e5911f55bd::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
