<?php


namespace InterNeto\ExternLinksModifier;

/**
 * Renders the admin settings page
 *
 * @package InterNeto\ExternLinksModifier
 */
class AdminSettings extends PluginComponent
{
    const OPTIONS_KEY = 'extern_links_modifier_options';

    protected $pageTitle = '';

    protected $options = [];

    public function __construct()
    {
        $this->pageTitle = __('External Links Modifier', 'extern-links-modifier');
        $this->options   = get_option(self::OPTIONS_KEY, []);

        add_action('admin_menu', [$this, 'initSettings']);
    }

    public function initSettings()
    {
        $capability = apply_filters('elm_admin_settings_capabilities', 'manage_options');

        add_submenu_page('options-general.php', $this->pageTitle, $this->pageTitle, $capability,
            'extern-links-modifier',
            [$this, 'renderSettings']);
    }

    public function renderSettings()
    {
        $this->processFormSubmit();

        extract([
            'title'      => $this->pageTitle,
            'post_types' => get_post_types(),
            'options'    => $this->options,
        ]);

        include_once __DIR__ . '/../templates/admin-settings.php';
    }

    protected function processFormSubmit()
    {
        if (empty($_POST) || ! wp_verify_nonce($_POST['_wpnonce'], 'extern_links_modifier')) {
            return;
        }

        $admin_options = $_POST['admin_options'];

        if (isset($admin_options['allowed_post_types'])) {
            $this->options['allowed_post_types'] = array_map('esc_sql', $admin_options['allowed_post_types']);
        }

        update_option(self::OPTIONS_KEY, $this->options);
    }

    public function onPluginSetup()
    {
        update_option(self::OPTIONS_KEY, [
            'allowed_post_types' => ['post', 'page']
        ]);

        return true;
    }

    /**
     * @return array|mixed|void
     */
    public function getOptions()
    {
        return $this->options;
    }
}