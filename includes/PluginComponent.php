<?php


namespace InterNeto\ExternLinksModifier;

/**
 * Wrapper for plugin components
 *
 * @package InterNeto\ExternLinksModifier
 */
abstract class PluginComponent
{
    /**
     * Called when plugin become active
     */
    public function onPluginSetup()
    {
        return true;
    }
}