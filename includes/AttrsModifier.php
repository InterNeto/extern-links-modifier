<?php


namespace InterNeto\ExternLinksModifier;

/**
 * Analyzes content links and setting up needed attributes to 'a' tags if it's needed.
 *
 * @package InterNeto\ExternLinksModifier
 */
class AttrsModifier extends PluginComponent
{
    /**
     * This property will contain the result of home_url() function without protocol
     *
     * @var string
     */
    protected $homeUrl = '';

    protected $options = [];

    public function __construct()
    {
        $this->homeUrl = parse_url(home_url(), PHP_URL_HOST);
        $this->options = Utils::getOptions();

        add_filter('the_content', [$this, 'parsePostLinks'], 10);
    }

    public function parsePostLinks($content)
    {
        global $post;

        $allowed_post_types = apply_filters('elm_allowed_post_types', $this->options['allowed_post_types']);
        if ( ! in_array($post->post_type, $allowed_post_types)) {
            return $content;
        }

        // With this filter, you can add your own content like post metadata
        $content = apply_filters('elm_content_to_parse', $content, $post);

        return $this->parseContentLinks($content) ?: $content;
    }

    public function parseContentLinks($content)
    {
        $doc = new \DOMDocument();
        $doc->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $links = $doc->getElementsByTagName('a');

        /**
         * @var $link \DOMElement
         */
        foreach ($links as $link) {
            $href = $link->getAttribute('href');

            if ($this->isExternalLink($href)) {
                $link->setAttribute('rel', 'nofollow');
                $link->setAttribute('target', '_blank');
            }
        }

        return $doc->saveHTML();
    }

    /**
     * Checks if this url is external
     *
     * @param $link
     *
     * @return bool
     */
    protected function isExternalLink($link)
    {
        $white_list = apply_filters('elm_no_external_links', [$this->homeUrl]);

        $url = parse_url($link, PHP_URL_HOST);
        if (in_array($url, $white_list)) {
            return false;
        }

        return true;
    }
}