<?php


namespace InterNeto\ExternLinksModifier;


class Utils
{
    /**
     * Return plugin options
     *
     * @return array
     */
    public static function getOptions() {
        return get_option(AdminSettings::OPTIONS_KEY, []);
    }
}