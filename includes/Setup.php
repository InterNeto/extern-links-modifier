<?php

namespace InterNeto\ExternLinksModifier;

class Setup
{
    /**
     * @var self|null
     */
    protected static $_instance = null;

    /**
     * @var AttrsModifier
     */
    protected $attrsModifier;

    /**
     * @var AdminSettings
     */
    protected $adminSettings;

    /**
     * @return Setup|null
     */
    public static function instance($plugin_path)
    {
        if (self::$_instance == null) {
            self::$_instance = new self($plugin_path);
        }

        return self::$_instance;
    }

    public function __construct($plugin_path)
    {
        $this->attrsModifier = new AttrsModifier();
        $this->adminSettings = new AdminSettings();

        register_activation_hook($plugin_path, [$this, 'initSetup']);
    }

    public function initSetup()
    {
        $this->attrsModifier->onPluginSetup();
        $this->adminSettings->onPluginSetup();
    }

    /**
     * @return AttrsModifier
     */
    public function getAttrsModifier()
    {
        return $this->attrsModifier;
    }

    /**
     * @return AdminSettings
     */
    public function getAdminSettings()
    {
        return $this->adminSettings;
    }
}