<?php
/*
Plugin Name: Extern Links Modify
Description: The plugin that adds nofollow and target attributes for 'a' tags.
Version: 0.0.1
Author: Victor Litvinchuk
License: MIT
Text Domain: extern-links-modifier
*/

require_once __DIR__ . '/vendor/autoload.php';

function ExternLinksModifier()
{
    return \InterNeto\ExternLinksModifier\Setup::instance(__FILE__);
}

// Initialize this plugin
ExternLinksModifier();