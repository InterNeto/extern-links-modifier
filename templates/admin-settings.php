<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo $title ?></h1>
    <form action="<?php echo admin_url('options-general.php') ?>?page=extern-links-modifier" method="post">
        <table class="form-table">
            <tbody>
            <?php if ( ! empty($post_types)): ?>
                <tr valign="top">
                    <th class="titledesc"><?php echo __('Allowed post types') ?></th>
                    <td class="forminp">
                        <fieldset>
                            <?php foreach ($post_types as $post_type): ?>
                                <div class="check-box">
                                    <label>
                                        <input type="checkbox"
                                               name="admin_options[allowed_post_types][]"
                                               value="<?php echo $post_type ?>"
                                            <?php checked(in_array($post_type, $options['allowed_post_types'])) ?>>
                                        <?php echo $post_type ?>
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </fieldset>
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?php wp_nonce_field('extern_links_modifier'); ?>
        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Update Options">
        </p>
    </form>
</div>